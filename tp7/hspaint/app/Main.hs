{-# LANGUAGE OverloadedStrings #-}

import           GI.Cairo.Render
import           GI.Cairo.Render.Connector (renderWithContext)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.Gtk.Objects.Widget
import           Data.IORef

type Point =(Int,Int)


f :: IORef Point -> IO()
f ref = writeIORef ref (0,0)

handleDraw :: Gtk.DrawingArea -> Render Bool
handleDraw canvas = do
    moveTo 0 0 
    lineTo 200 100
    stroke
    return True

handlePress :: IORef Point -> Gdk.EventButton -> IO Bool
handlePress ref b= do
    --Gtk.mainQuit
   -- modifyIORef ref ()

    return True



main :: IO ()
main = do

    ref<-newIORef (20,7)
    f ref

    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    canvas <- Gtk.drawingAreaNew
    _ <- Gtk.onWidgetDraw canvas $ renderWithContext $ handleDraw canvas
    Gtk.containerAdd window canvas 

    Gtk.widgetSetEvents canvas [ Gdk.EventMaskButtonPressMask ]
    _ <- onWidgetButtonPressEvent canvas (handlePress ref)

    coord<-readIORef ref
    print(coord)

    Gtk.widgetShowAll window
    Gtk.main

