import System.Random
--import Data.Time

listeMot :: [String]
listeMot = ["function","functional programming","haskell","list","pattern matching","recursion","tuple","type system"]

--time::IO UTCTime
--time = getCurrentTime

seed :: Int
seed=1

generator :: StdGen
generator = mkStdGen seed

motAléatoire :: String
motAléatoire = listeMot !! rand where
    n = length listeMot
    (rand,_) = randomR (0,(n-1)) generator

convertionMot :: String -> String
convertionMot [] = ""
convertionMot (x:reste) = if x == ' ' 
                                then ' ' : convertionMot reste
                                else '?' : convertionMot reste


jouerCoup :: String -> IO String
jouerCoup coup = do
    putStr">"
    msg<-getLine
    if msg ==""
        then return coup
        else do 
            putStrLn(concat(coup,msg))
            jouerCoup (concat(coup,msg))



main :: IO ()
main = do 
    putStrLn "+-------+\n\
             \| hello |\n\
             \+-------+"
    let mot = motAléatoire
    print mot
    print (convertionMot mot)
    jeu <- jouerCoup ""
    putStrLn(jeu)