myFst :: (a,b) -> a
myFst (x,_) = x

mySnd :: (a,b) -> b
mySnd (_,x) = x 

main :: IO()
main = do
    print (myFst (1,2))
    print (myFst ("truc",1))
    print (mySnd ("truc",1))