fuzzyLength :: [a] -> String
fuzzyLength [] = "empty"
fuzzyLength [x1] = "one"
fuzzyLength [x1,x2] = "two"
fuzzyLength xs = "many"

main :: IO()
main = do
    print(fuzzyLength [1,2])