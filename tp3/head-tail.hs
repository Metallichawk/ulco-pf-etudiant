safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail [_] = Nothing
safeTail (_:x) = Just x

main :: IO()
main = do 
    print (safeHead "")
    print (safeHead "FooFighter")