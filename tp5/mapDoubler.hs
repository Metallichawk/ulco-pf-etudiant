mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = x*2 : mapDoubler1 xs

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 x = map(*2) x

main :: IO()
main = do
    print(mapDoubler1([1,2,3]))
    print(mapDoubler2([1,2,3]))