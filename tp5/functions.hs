doubler :: [Int] -> [Int]
doubler xs = [ x*2 | x<-xs ]

pairs :: [Int] -> [Int]
pairs xs = [ x | x<-xs, even x]

multiples :: Int -> [Int]
multiples n = [ x | x<- [1..n], n `mod` x==0]

combinaison :: [a] -> [b] ->[(a,b)]
combinaison a b = [ (x,y) | x<-a,y<-b]

triplesPyth :: Int -> [Int]
triplesPyth n = [ (x,y,z) | x<-[1..n]
                           ,y<-[1..n]
                           ,z<-[1..n]
                           ,x*x+y*y=z*z]

main :: IO()
main = do 
    print(doubler [1..4])
    print(pairs [1..4])