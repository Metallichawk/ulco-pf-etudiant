mymap :: (a, b) -> [a] -> [b]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs

main :: IO()
main = do 
    print(mymap (*2) [1..4])