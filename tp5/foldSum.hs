foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 (x:xs) = foldl (+) x xs

main :: IO()
main = do 
    print(foldSum1 [1..4])
    print(foldSum2 [1..4])