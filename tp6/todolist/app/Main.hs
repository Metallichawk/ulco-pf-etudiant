{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
import Data.Text

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 500 300
    Gtk.windowSetTitle window "Hello World!"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    box <- Gtk.boxNew Gtk.OrientationVertical 10 
    Gtk.containerAdd window box

    text <- Gtk.dialogNew
    Gtk.containerAdd box text

    btnAdd <- Gtk.buttonNew
    Gtk.setButtonLabel btnAdd "Add"
    Gtk.containerAdd box btnAdd

    btnClear <- Gtk.buttonNew
    Gtk.setButtonLabel btnClear "Clear"
    Gtk.containerAdd box btnClear

    label <- Gtk.labelNew (Just "TODO LIST")
    Gtk.containerAdd box label

    Gtk.widgetShowAll window
    Gtk.main

