main :: IO()
main = do 
    print(map (\x -> x*2) [1..4])
    print(map (\x -> x `div` 2) [1..4])
    print(map (\(x,y) -> show x++"-"++ show y)(zip ['a'..'z'][1..]))
    print(zipWith (\c i -> c : "-" ++ show i) ['a'..'z'][1..])