plus42 x = x+42
positif y = y>0

plus42positif :: Int -> Bool
plus42positif = (>0) . (+42)

mul2plus1 :: Int -> Int
mul2plus1 = (+1) . (*2)

mul2moins1 :: Int -> Int
mul2moins1 = (+(-1)) . (*2)

main :: IO()
main = do
    print(plus42positif 2)