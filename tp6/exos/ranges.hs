myRangeTuples1 :: (Int,Int) -> [Int]
myRangeTuples1 (x,y) = [x..y]

myRangeCurry1 :: Int -> Int -> [Int]
myRangeCurry1 x y = [x..y]

myRangeTuples2 :: Int -> [Int]
myRangeTuples2 x = myRangeTuples1 (0,x)

myRangeCurry2 :: Int -> [Int]
myRangeCurry2 x = myRangeCurry1 0 x

main :: IO()
main = do
    print(myRangeTuples1(1,9))
    print(myRangeCurry1 1 9)
    print(myRangeTuples2 9)
    print(myRangeCurry2 9)
                    