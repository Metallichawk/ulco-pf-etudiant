myTake2 :: [a] -> [a]
myTake2 = take 2

myGet2 :: [a] -> a
myGet2 = (flip(!!)) 3

main :: IO()
main = do
    print(myTake2 [1..4])