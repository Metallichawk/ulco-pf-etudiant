facto :: Int -> Int
facto 1 = 1
facto x = x * facto(x-1)

main :: IO()
main = print(facto 2)