loopEcho :: Int -> IO String
loopEcho 0 = return"fin boucle"   
loopEcho x = do
    putStr ">"
    msg <- getLine
    if msg == "" 
        then return "message empty"
        else do 
            putStrLn(msg)
            loopEcho(x-1)

main :: IO()
main = do 
    result <- loopEcho 2
    putStrLn(result)