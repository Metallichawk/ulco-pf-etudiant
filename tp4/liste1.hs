import Data.Char

mylength :: [a]->Int
mylength [_] = 1
mylength (_:xs) = 1 + mylength xs

toUpperString :: String -> String
toUpperString [] = ""
toUpperString (x:xs) = toUpper x : toUpperString xs

onlyLetter :: String -> String
onlyLetter "" = ""
onlyLetter (x:xs) = 
    if isLetter x == True
        then x : onlyLetter xs
        else onlyLetter xs

main :: IO()
main = do 
    print(mylength "foobar")
    print(toUpperString "foo bar")