mulListe :: Int -> [Int] -> [Int]
mulListe n [] = []
mulListe n (x:xs) = n*x : mulListe n xs

main :: IO()
main = do
    print(mulListe 2 [1,2,3])
