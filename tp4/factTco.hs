factTco :: Int -> Int
factTco n = aux n 1
    where aux 0 acc = acc
          aux n acc = aux (n-1) (n*acc)

main :: IO()
main = do
    print (factTco 5)