formaterParite :: Int -> String
formaterParite x = if even x
        then "pair"
        else "impair"

formaterSigne :: Int -> String
formaterSigne x = if x>0 
    then "+"
    else if x<0
        then "-"
        else "0"

main :: IO()
main = do
    putStrLn (formaterParite 20)
    putStrLn (formaterParite 653)
    putStrLn (formaterSigne 7)
    putStrLn (formaterSigne (-7))
    putStrLn (formaterSigne 0)