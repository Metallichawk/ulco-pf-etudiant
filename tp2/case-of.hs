fibo :: Int -> Int
fibo x = case x of
    0 -> 1
    1 -> 1
    _ -> fibo(x-1) + fibo(x-2)

