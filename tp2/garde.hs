formaterParite :: Int -> String
formaterParite x 
    | even x = "pair"
    | otherwise = "impair"

formaterSigne :: Int -> String
formaterSigne x
    | x>0 = "+"
    | x<0 = "-"
    | otherwise = "0"

main :: IO()
main = do
    putStrLn (formaterParite 20)
    putStrLn (formaterParite 21)
    putStrLn (formaterSigne 21)
    putStrLn (formaterSigne (-21))
    putStrLn (formaterSigne 0)
    