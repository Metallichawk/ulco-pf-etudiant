import Text.Read (readMaybe)

main :: IO()
main = do
    input <- getLine
    let truc = readMaybe(input)
    print (truc::Maybe Int)