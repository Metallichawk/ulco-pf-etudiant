borneEtFormate1 :: Double -> String
borneEtFormate1 x0 = show x0 ++ " -> " ++ show x1
    where x1 = if x0 < 0
               then 0
               else if x0 > 1
                    then 1
                    else x0

borneEtFormate2 :: Double -> String
borneEtFormate2 x0 = show x0 ++ " -> " ++ show x1
        where x1 | x0 < 0 = 0
                 | x0 > 1 = 1
                 | otherwise = x0

main :: IO()
main = do
    putStrLn (borneEtFormate1 (0.7))
    putStrLn (borneEtFormate2 (2))
    