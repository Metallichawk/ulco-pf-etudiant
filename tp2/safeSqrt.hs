safeSqrt :: Double -> Maybe Double
safeSqrt x = if x < 0 then Nothing else Just(sqrt x)

formatedSqrt :: Double -> String
formatedSqrt x =
    case safeSqrt x of
        Nothing -> "sqrt ("++show x++") is not defined"
        Just res -> "sqrt ("++show x++") = "++ show res

main :: IO()
main = do
    print(safeSqrt 4)
    print(safeSqrt (-7))
    print(formatedSqrt 7)
    print(formatedSqrt (-7))